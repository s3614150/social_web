class Article < ApplicationRecord
   belongs_to :user
    has_many :comments
    validates :title, presence: true, length: { maximum: 50 }
    validates :post, presence: true
    validate :picture_size
    
    mount_uploader :picture, PictureUploader
    default_scope -> { order(created_at: :desc) } 
    
    private 
    
    # Validates the size of an uploaded picture.
    def picture_size
        if picture.size > 5.megabytes
            errors.add(:picture, "should be less than 5MB")
        end
    end
end
