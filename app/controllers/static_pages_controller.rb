class StaticPagesController < ApplicationController
  before_action :set_page, only: [:index]
  
  ARTICLES_PER_PAGE = 8
  
  def home
    @articles = Article.all
  end
  
  def index 
    @articles = Article.order(:id).limit(ARTICLES_PER_PAGE).offset(@page * ARTICLES_PER_PAGE)
  end
  
  def comments
  end

  def submit
  end

  def login
  end
  
  private
  def set_page
    @page = params[:page].to_i || 0
  end
  
  def about
  end
  
  def help
  end
  
end

