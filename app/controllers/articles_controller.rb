class ArticlesController < ApplicationController
  before_action :logged_in_user
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @article = Article.find(params[:id])
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  def create
    @article = current_user.articles.build(article_params)
    if @article.save
      flash[:success] = "Article Created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  
    
    # @article = Article.new(article_params)

    # respond_to do |format|
    #   if @article.save
    #     format.html { redirect_to @article, notice: 'Article was successfully created.' }
    #     format.json { render :show, status: :created, location: @article }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @article.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end


  private

    def article_params
      params.require(:article).permit(:content)
    end
    
    def set_article
      @article = Article.find(params[:id])
    end

    def article_params
      params.require(:article).permit(:title, :post, :user_id)
    end
    
  # confirms a logged-in user.
  def logged_in_user
    unless logged_in?
    flash[:danger] = "Please log in to be able to submit"
    redirect_to login_url
    end
  end
  
  def correct_user
    @user = current_user
    redirect_to(root_url) unless @user == current_user
  end
  
  def set_page
    @page = params[:page].to_i || 0
  end
end
