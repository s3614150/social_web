class CommentsController < ApplicationController
    before_action :logged_in_user
    
    def index
        @comments = Comment.all
    end
    
    def new
        @comment = Comment.new
        
    end
    
    def create
    @article = Article.find(params[:article_id])
    
    @comment = @article.comments.create(comment_params)
    
    redirect_to article_path(@article)
    end
    
    def home
        @articles = Article.all
    end
    
    def comments
    end
    
    private
    
    def set_comments
        @comment = Comment.find(params[:id])
    end
    
    def comment_params
        params.require(:comment).permit(:commenter, :body)
    end
end
